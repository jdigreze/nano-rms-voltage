#include <TimerOne.h> // from https://github.com/PaulStoffregen/TimerOne

#define DBG_PRINT(...) { Serial.print(__VA_ARGS__); }
#define DBG_PRINTLN(...) { Serial.println(__VA_ARGS__); }

#define LED LED_BUILTIN
#define ZERO_OFFSET_A0 512 // 580 for 4.096V or 512 for 5.0V
#define ZERO_OFFSET_A1 512
#define SAMPLERATE  2048
#define BUFFER_LENGTH 256
#define SCALE 512
#define UREF 0.004882813F // for 5.0V or 0.004F for 4.096V

volatile int16_t buffer_in[BUFFER_LENGTH];
volatile int16_t buffer_out[BUFFER_LENGTH];
volatile uint16_t ptr = 0;
volatile bool flag = false;
volatile uint8_t currentChannel = 0;
volatile double rms_in = 0;
volatile double rms_out = 0;

void setup() {
  //analogReference(EXTERNAL); // uncomment for external AREF 
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(LED, OUTPUT);
  Serial.begin(115200);
  Timer1.initialize(1000000/(SAMPLERATE*2));
  Timer1.attachInterrupt(getRawAdc);
}

void getRawAdc() {
  int16_t raw;
  if ( flag ) return;

  if ( currentChannel == 0 ) {
    raw = analogRead(A0) - ZERO_OFFSET_A0;
    buffer_in[ptr] = raw;
    rms_in += pow((double)raw * UREF, 2);
    currentChannel++;
  } else {
    raw = analogRead(A1) - ZERO_OFFSET_A1;
    buffer_out[ptr] = raw;
    rms_out += pow((double)raw * UREF, 2);
    ptr++;
    currentChannel = 0;
  }
  if ( ptr > BUFFER_LENGTH ) {
    flag = true;
    ptr = 0;
  }
}

void loop() {
  if ( flag ) {
    double u_in = sqrt(rms_in / BUFFER_LENGTH) * SCALE;
    double u_out = sqrt(rms_out / BUFFER_LENGTH) * SCALE;
    for ( ptr = 0; ptr < BUFFER_LENGTH; ptr++ ) {
      DBG_PRINT(u_in);
      DBG_PRINT('\t');
      DBG_PRINT(u_out);
      DBG_PRINT('\t');
      DBG_PRINT(buffer_in[ptr]);
      DBG_PRINT('\t');
      DBG_PRINTLN(buffer_out[ptr]);
    }
    rms_in = 0;
    rms_out = 0;
    delay(1000);
    ptr = 0;
    flag = false;
  }
}
